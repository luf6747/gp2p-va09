package studiplayer.audio;

import java.util.Comparator;

public class DurationComparator implements Comparator<AudioFile> {
    public int compare(AudioFile af1, AudioFile af2) {
        int ret_val;
        if (af1 == null || af2 == null) {
            throw new NullPointerException("DurationComparator class cant't compare to null pointer.");
        } else if (af1 instanceof SampledFile && !(af2 instanceof SampledFile)) {
            ret_val = 1;
        } else if (!(af1 instanceof SampledFile) && af2 instanceof SampledFile) {
            ret_val = -1;
        } else if (!(af1 instanceof SampledFile) && !(af2 instanceof SampledFile)) {
            ret_val = 0;
        } else {
            // both objects have a duration (and are a SampledFile), do a
            // compare of those durations:
            SampledFile sf1 = (SampledFile) af1;
            SampledFile sf2 = (SampledFile) af2;
            ret_val = sf1.getFormattedDuration().compareTo(sf2.getFormattedDuration());
        }
        return ret_val;
    }
}
