package studiplayer.audio;

import java.util.Comparator;

public class AlbumComparator implements Comparator<AudioFile> {
    public int compare(AudioFile af1, AudioFile af2) {
        int ret_val;
        if (af1 == null || af2 == null) {
            throw new NullPointerException("AlbumComparator class cant't compare to null pointer.");
        } else if (af1 instanceof TaggedFile && !(af2 instanceof TaggedFile)) {
            ret_val = 1;
        } else if (!(af1 instanceof TaggedFile) && af2 instanceof TaggedFile) {
            ret_val = -1;
        } else if (!(af1 instanceof TaggedFile) && !(af2 instanceof TaggedFile)) {
            ret_val = 0;
        } else {
            // both objects have a album (and are a TaggedFile), do a compare of
            // those Albums:
            TaggedFile tf1 = (TaggedFile) af1;
            TaggedFile tf2 = (TaggedFile) af2;
            ret_val = tf1.getAlbum().compareTo(tf2.getAlbum());
        }
        return ret_val;
    }
}
